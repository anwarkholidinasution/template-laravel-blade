
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>form.html</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome">
		<label for="Fname">First name:</label> <br><br>
		<input type="text" id="Fname" name="Fname"> <br><br>
		<label for="Lname">Last name:</label> <br> <br>
		<input type="text" id="Lname" name="Lname">

		<p>Gender:</p>
		<input type="radio" id="Male" name="Male">
		<label for="Male">Male</label> <br>
		<input type="radio" id="Female" name="Female">
		<label for="Female">Female</label> <br>
		<input type="radio" id="Other" name="Other">
		<label for="Other">Other</label> <br> <br>

		<label>Nationality:</label> <br> <br>
		<select name="nationality">
                <option value="indonesia">Indonesian</option>
                <option value="malaysia">Malaysian</option>
                <option value="japan">Japanese</option>
                <option value="korea">Korean</option>
        </select> <br>

        <p>Language Spoken:</p>
        <input type="checkbox" id="Bahasa Indonesia" name="Bahasa Indonesia">
        <label for="Bahasa Indonesia">Bahasa Indonesia</label> <br>
        <input type="checkbox" id="English" name="English">
        <label for="English">English</label> <br>
        <input type="checkbox" id="other" name="other">
        <label for="other">Other</label> <br> <br>

        <label>Bio:</label> <br>
        <textarea name="Bio" rows="10" cols="50"></textarea> <br>
        <input type="submit" value="Sign Up">
	</form>
</body>
</html>